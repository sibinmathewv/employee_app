import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:employee_info/bloc/employee_bloc.dart';

const _rowPadding = EdgeInsets.all(16);

class EmployeeDisplay extends StatefulWidget {
  const EmployeeDisplay({Key? key}) : super(key: key);

  @override
  _EmployeeDisplayState createState() => _EmployeeDisplayState();
}

class _EmployeeDisplayState extends State<EmployeeDisplay> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Employee Details'),
      ),
      body: BlocBuilder<EmployeeBloc, EmployeeState>(
          builder: _viewEmployeeBuilder),
    );
  }

  Widget _viewEmployeeBuilder(BuildContext context, EmployeeState state) {
    switch (state.runtimeType) {
      case EmployeeAddedState:
        return _viewEmployeeInfo(context, state as EmployeeAddedState);
      default:
        return Container();
    }
  }

  Widget _viewEmployeeInfo(BuildContext context, EmployeeAddedState state) {
    return Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: _rowPadding,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text('Name : '),
                    Text(state.name),
                  ],
                ),
              ),
              Padding(
                padding: _rowPadding,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text('Employee Id : '),
                    Text(state.employeeId),
                  ],
                ),
              ),
              Padding(
                padding: _rowPadding,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text('email : '),
                    Text(state.email),
                  ],
                ),
              ),
              Padding(
                padding: _rowPadding,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text('Phone number : '),
                    Text(state.phoneNumber),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
