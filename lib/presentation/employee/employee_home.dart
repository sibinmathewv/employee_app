import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:employee_info/bloc/employee_bloc.dart';
import 'package:employee_info/presentation/employee/employee_display.dart';
import 'package:employee_info/utils/textfield_validator.dart';

const _heightFifteen = SizedBox(height: 15);
const _formPadding = EdgeInsets.all(16);
const TextStyle _textFieldSize = TextStyle(fontSize: 16);
const TextStyle _buttonFontSize = TextStyle(fontSize: 16);

class EmployeeHomePage extends StatefulWidget {
  const EmployeeHomePage({Key? key}) : super(key: key);

  @override
  _EmployeeHomePageState createState() => _EmployeeHomePageState();
}

class _EmployeeHomePageState extends State<EmployeeHomePage> {
  TextEditingController employeeNameController = TextEditingController();
  TextEditingController employeeEmailController = TextEditingController();
  TextEditingController employeePhoneController = TextEditingController();
  TextEditingController employeeIdController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Employee App'),
        centerTitle: true,
      ),
      body: _employeeForm(context),
    );
  }

  Widget _employeeForm(BuildContext context) {
    return Padding(
      padding: _formPadding,
      child: Column(
        children: [
          Expanded(
            child: Form(
              key: formKey,
              child: ListView(
                  controller: ScrollController(),
                  shrinkWrap: true,
                  children: [
                    TextFormField(
                        controller: employeeNameController,
                        style: _textFieldSize,
                        decoration: const InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Name',
                        ),
                        validator:
                            TextFieldValidator(context: context).nameValidator),
                    _heightFifteen,
                    TextFormField(
                      enableInteractiveSelection: false,
                      controller: employeePhoneController,
                      keyboardType: TextInputType.phone,
                      style: _textFieldSize,
                      validator:
                          TextFieldValidator(context: context).phoneValidator,
                      decoration: const InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Phone number'),
                    ),
                    _heightFifteen,
                    TextFormField(
                        enableInteractiveSelection: false,
                        controller: employeeEmailController,
                        style: _textFieldSize,
                        decoration: const InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Email address',
                        ),
                        validator: TextFieldValidator(context: context)
                            .emailValidator),
                    _heightFifteen,
                    TextFormField(
                        controller: employeeIdController,
                        style: _textFieldSize,
                        decoration: const InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Employee Id',
                        ),
                        validator: TextFieldValidator(context: context)
                            .employeeIdValidator),
                    _heightFifteen,
                    _heightFifteen,
                    ElevatedButton(
                      style:
                          ElevatedButton.styleFrom(textStyle: _buttonFontSize),
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BlocProvider<EmployeeBloc>(
                                create: (context) => EmployeeBloc()
                                  ..add(
                                    EmployeeSubmitButtonClickedEvent(
                                        name: employeeNameController.text,
                                        employeeId: employeeIdController.text,
                                        email: employeeEmailController.text,
                                        phoneNumber:
                                            employeePhoneController.text),
                                  ),
                                child: const EmployeeDisplay(),
                              ),
                            ),
                          );
                        }
                      },
                      child: const Text('Save'),
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
