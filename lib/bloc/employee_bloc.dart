import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'employee_event.dart';
part 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  EmployeeBloc() : super(EmployeeInitial())
  {
  on<EmployeeSubmitButtonClickedEvent>((event, emit) async {
  emit(EmployeeAddedState(
            name: event.name,
            email: event.email,
            phoneNumber: event.phoneNumber,
            employeeId: event.employeeId));
});}

}
