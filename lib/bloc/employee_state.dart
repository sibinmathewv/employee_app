part of 'employee_bloc.dart';

@immutable
abstract class EmployeeState {}

class EmployeeInitial extends EmployeeState {}

class EmployeeAddedState extends EmployeeState {
  EmployeeAddedState(
      {required this.name,
      required this.phoneNumber,
      required this.email,
      required this.employeeId});
  final String name;
  final String phoneNumber;
  final String email;
  final String employeeId;
}
