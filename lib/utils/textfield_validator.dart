import 'package:flutter/cupertino.dart';

class TextFieldValidator {
  final BuildContext context;

  TextFieldValidator({required this.context});

  String? emailValidator(String? value) {
    if (value!.isEmpty || value.trim() == '') {
      return 'Enter email id';
    } else if (!RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(value)) {
      return 'Invalid email';
    }
  }

  String? phoneValidator(String? value) {
    if (value!.isEmpty || value.trim() == '') {
      return 'Enter phone number';
    } else if (value.length < 8) {
      return 'Incorrect phone number';
    } else if (RegExp(r'^[0-9]{3}-[0-9]{3}-[0-9]+$').hasMatch(value)) {
      return null;
    }
  }

  String? nameValidator(String? value) {
    if (value!.isEmpty || value.trim() == '') {
      return 'Enter name';
    } else if (RegExp(r'^[a-z A-Z ]+$').hasMatch(value)) {
      return null;
    }
    return 'Enter name';
  }

  String? employeeIdValidator(String? value) {
    if (value!.isEmpty || value.trim() == '') {
      return 'Enter Empleyee Id';
    } else if (value.length < 6) {
      return 'Min 6 Character required';
    } else if (value.length > 6) {
      return 'Mix 6 Character allowed';
    } else {
      return null;
    }
  }
}
